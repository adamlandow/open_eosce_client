export class Config {
    service_url: string = '';
    name: string = 'Not connected';
    client_id: string = '';
    client_secret: string = '';
  }