import { Injectable } from '@angular/core';
//import { Observable } from "tns-core-modules/data/observable";
import { Observable, of } from 'rxjs';
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Config } from '../models/config.model';
import { Couchbase } from 'nativescript-couchbase';
import { RouterExtensions } from 'nativescript-angular/router';



const httpOptions = {
  headers: new HttpHeaders({

    'Accept': 'application/json',
    'Content-Type': 'application/json',

  })
};

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  config: Config;
  storageDatabase: Couchbase;

  url = '';
  headers = new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  });

  constructor(private http: HttpClient, private routerExtensions: RouterExtensions) {
    this.storageDatabase = new Couchbase('storage');
    this.config = this.storageDatabase.getDocument("config")
    if (!this.config) {
      this.routerExtensions.navigate(['config']);
    }
  }

  isAValidUrl(value: string): boolean {
    try {
      const url = new URL(value);
      return true;
    } catch (TypeError) {
      return false;
    }
  }

  login(username, password): Observable<object> {
    console.log('login.service.ts says: login() called')
    // check the configuration
    if (this.isAValidUrl(this.config.service_url)) {
      let formData = JSON.stringify({
        username: username,
        password: password,
        grant_type: 'password',
        client_id: this.config.client_id,
        client_secret: this.config.client_secret
      })
    }else{
      this.routerExtensions.navigate(['config']);
    }

    // console.log(formData);
    return this.http.post(this.config.service_url + '/oauth/token', formData, { headers: this.headers })
  }
}
