/**
 * Maybe this holds information about the currently logged in user. A service makes it volatile, rather than holding 
 * information (including auth tokens) in persistent storage
 */
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public user: User;
  
  constructor() { }

  // setUser(user: User) {
  //   this.user = user;
  // }

  // getUser() {
  //   return (this.user) ? this.user : new User();
  // }

}
