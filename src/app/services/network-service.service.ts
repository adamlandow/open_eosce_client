import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Config } from '../models/config.model';
import { Couchbase } from 'nativescript-couchbase';
import { RouterExtensions } from 'nativescript-angular/router';
import { User } from '../models/user.model';
import { Assessment } from '../models/assessment.model';

@Injectable({
  providedIn: 'root'
})
export class NetworkServiceService {
  config: Config = new Config();
  storageDatabase: Couchbase;
  access_token: string;

  headers = new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  });

  constructor(private http: HttpClient, private routerExtensions: RouterExtensions) {
    this.storageDatabase = new Couchbase('storage');
    // get config- if not, go to setup page
    if (this.storageDatabase.getDocument("config")) {
      this.config = this.storageDatabase.getDocument("config");
    } else {
      this.routerExtensions.navigate(['config']);
    }

  }

  // login, get token
  login(username: string, password: string): Observable<object> {
    console.log('network-service.service.ts says: login() called')
    let formData = JSON.stringify({
      username: username,
      password: password,
      grant_type: 'password',
      client_id: this.config.client_id,
      client_secret: this.config.client_secret
    })
    // console.log(formData);
    return this.http.post(this.config.service_url + '/oauth/token', formData, { headers: this.headers })
  }

  // check that the configured service is correct
  checkConfig(): Observable<object> {
    if (this.storageDatabase.getDocument("config")) {
      this.config = this.storageDatabase.getDocument("config");
    } else {
      this.routerExtensions.navigate(['config']);
    }
    console.log('network-service.service.ts says: checkConfig() called')
    let formData = JSON.stringify(this.config);
    return this.http.post(this.config.service_url + '/api/check', formData, { headers: this.headers })
      ;
  }

  // get the logged on user details from the backend
  // @TODO offline capability- store then 
  getUserDetails(): Observable<object> {
    console.log('network-service.service.ts says: getUserDetails() called')
    let formData = JSON.stringify(this.config);
    this.headers.append('Authorization', 'Bearer ' + this.access_token)
    console.log('Token:' + this.access_token);
    let theheaders = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.access_token
    });
    return this.http.post(this.config.service_url + '/api/user', formData, { headers: theheaders })
      ;
  }

  // get the exams assigned to this user
  // @TODO offline capability- store then get from database if not connected
  getUserAssessments(): Observable<any> {
    console.log('network-service.service.ts says: getUserAssessments() called')

    this.headers.append('Authorization', 'Bearer ' + this.access_token)
    console.log('Token:' + this.access_token);
    let theheaders = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.access_token
    });
    return this.http.post(this.config.service_url + '/api/getassessments', null, { headers: theheaders })
      ;
  }

  // get a specific exam
  // @TODO offline capability- store then get from database if not connected
  getAssessment(id: string): Observable<any> {
    console.log('network-service.service.ts says: getAssessment() called')
    this.headers.append('Authorization', 'Bearer ' + this.access_token)
    console.log('Token:' + this.access_token);
    let theheaders = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.access_token
    });
    return this.http.post(this.config.service_url + '/api/getassessmentdetails/' + id, null, { headers: theheaders })
      ;
  }

  // get the students assigned to this assessment
  // @TODO offline capability- get from database if not connected
  getstudentsForAssessment(id: string): Observable<any> {
    console.log('network-service.service.ts says: getstudentsForAssessment() called')
    this.headers.append('Authorization', 'Bearer ' + this.access_token)
    console.log('Token:' + this.access_token);
    let theheaders = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.access_token
    });
    return this.http.post(this.config.service_url + '/api/getassessmentstudents/' + id, null, { headers: theheaders })
      ;
  }

  // Submit an assignment
  // @TODO offline capability- set to database if not connected, upload when connection detected
  submitAssessment(student_id: string, assessment: Assessment): Observable<any> {
    console.log('network-service.service.ts says: submitAssessment() called')
    // build the answer object
    let submitdata = {
      student_id: student_id,
      exam_instances_id: assessment.id,
      comments: assessment.comment,
      answerdata: []
    };

    // answers
    assessment.exam_instance_items.forEach(element => {
      submitdata.answerdata.push({
        id: element.id,
        value: element.selectedvalue,
        selected_id: element.selected_id,
        comment: element.comment
      })
    }
    );

    // auth header
    this.headers.append('Authorization', 'Bearer ' + this.access_token)
    let theheaders = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.access_token
    });
    // submit

    return this.http.post(this.config.service_url + '/api/submitassessment', submitdata, { headers: theheaders, responseType: 'text' })
      ;
  }

}
