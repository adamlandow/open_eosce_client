import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from './components/login/login.component';
import { ConfigComponent } from "./components/config/config.component";
import { ExamlistComponent } from "./components/exam/examlist/examlist.component";
import { HttpClientModule} from '@angular/common/http';

export function createBarcodeScanner() {
    return new BarcodeScanner();
  }
// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpModule } from "nativescript-angular/http";
import { registerElement } from "nativescript-angular/element-registry";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { UserService } from "./services/user.service";
import { ExamitemComponent } from './components/exam/examdetail/examitem/examitem.component';
import { ExamdetailComponent } from "./components/exam/examdetail/examdetail.component";
import { TNSCheckBoxModule } from 'nativescript-checkbox/angular';
import { NgShadowModule } from 'nativescript-ng-shadow';
import { SelectstudententryComponent } from './components/exam/examdetail/modals/studentlist/selectstudententry/selectstudententry.component';
import { SubmitconfirmComponent } from './components/exam/examdetail/modals/submitconfirm/submitconfirm.component';
import { StudentlistComponent } from './components/exam/examdetail/modals/studentlist/studentlist.component';
import { CommentEntryComponent } from './components/exam/examdetail/modals/comment-entry/comment-entry.component';

registerElement("BarcodeScanner", () => require("nativescript-barcodescanner").BarcodeScannerView);


@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptFormsModule,
        HttpClientModule,
        TNSCheckBoxModule,
        NgShadowModule,
 
    ],
    // entryComponents holds references to modal components
    entryComponents: [StudentlistComponent,
    CommentEntryComponent],
    declarations: [
        AppComponent,
        LoginComponent,
        ConfigComponent,
        ExamlistComponent,
        ExamitemComponent,
        ExamdetailComponent,
        SelectstudententryComponent,
        SubmitconfirmComponent,
        StudentlistComponent,
        CommentEntryComponent
    ],
    providers: [
        { provide: BarcodeScanner, useFactory: (createBarcodeScanner) },
        UserService
      ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
