import { Component, OnInit } from '@angular/core';
import { User } from '~/app/models/user.model';
import { UserService } from '~/app/services/user.service';
import { Assessment } from '~/app/models/assessment.model';
import { NetworkServiceService } from '~/app/services/network-service.service';
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';
import { Page, EventData } from "tns-core-modules/ui/page";
import {LoadingIndicator} from "nativescript-loading-indicator";

@Component({
  selector: 'ns-examlist',
  templateUrl: './examlist.component.html',
  styleUrls: ['./examlist.component.css'],
  moduleId: module.id,
})
export class ExamlistComponent implements OnInit {

  //public assessmentlist: ObservableArray<Assessment>= new ObservableArray();
  // public assessmentlist: Assessment[] = [{id:'1',
  //   name:"an exam",description:"description",status:"active", items:[]},
  //   {id:'2',name:"another exam",description:"description",status:"active", items:[]},
  //   {id:'2',name:"another exam",description:"description",status:"active", items:[]},
  //   {id:'2',name:"another exam",description:"description",status:"active", items:[]},
  //   {id:'2',name:"another exam",description:"description",status:"active", items:[]},
  //   {id:'2',name:"another exam",description:"description",status:"active", items:[]},
  //   {id:'2',name:"another exam",description:"description",status:"active", items:[]},
  //   {id:'2',name:"another exam",description:"description",status:"active", items:[]},
  //   {id:'2',name:"another exam",description:"description",status:"active", items:[]},];

 
  public assessmentlist: Assessment[] = [];

  isBusy: Boolean = true;

  loader = new LoadingIndicator();
  options = {
    message: 'Loading your exams',
    android: {
      indeterminate: true,
      cancelable: true,
      cancelListener: function(dialog) { console.log("Loading cancelled") },
      
    },
    ios: {
      details: "Please wait...",
      margin: 10,
      dimBackground: true,
      color: "#4B9ED6", // color of indicator and labels
      // background box around indicator
      // hideBezel will override this if true
     // backgroundColor: "yellow",
      //userInteractionEnabled: false, // default true. Set false so that the touches will fall through it.
   //   hideBezel: true, // default false, can hide the surrounding bezel
     // view: UIView, // Target view to show on top of (Defaults to entire window)
    //  mode:  MBProgressHUDMod.Determinate// see iOS specific options below
    }
  };

  constructor(public userservice: UserService, private networkService: NetworkServiceService) {
    console.log('examlist.component.ts constructor called')
  }

  ngOnInit() {
    console.log('examlist.component.ts ngOnInit called')
    //console.log(JSON.stringify(this.userservice.user));
    this.loader.show(this.options);
    this.getAssessments()
  }

  getAssessments() {
    this.isBusy = true;
    console.log("examilst.component.ts says: getAssessment() called");
    this.networkService.getUserAssessments()
      .subscribe(result => {
        this.loader.hide();
        result.forEach(element => {
          // console.log('Element:'+JSON.stringify(element));
          this.assessmentlist.push(element);
         
        });
       
console.log('assessment list is:'+JSON.stringify(this.assessmentlist));
      }),
      error => console.log(error),
      () => console.log('login.component.ts.login() subscription complete result:complete');
      this.loader.hide();
  }

}
