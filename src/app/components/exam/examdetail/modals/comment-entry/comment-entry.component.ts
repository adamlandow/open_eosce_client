import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalDialogParams } from 'nativescript-angular/modal-dialog';
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";
import { EventData } from 'tns-core-modules/ui/page/page';

@Component({
  selector: 'ns-comment-entry',
  templateUrl: './comment-entry.component.html',
  styleUrls: ['./comment-entry.component.css'],
  moduleId: module.id,
})



export class CommentEntryComponent implements OnInit {

  @ViewChild('#comment_entry') comment_entry: ElementRef;

  private comment: string = "";


  constructor(private params: ModalDialogParams) {
    console.log('Incoming Comment is:' + this.comment);
  }

  public close(result: string) {
    console.log('outgoing Comment is:' + result);
    this.params.closeCallback(result);
  }

  public onload(args:EventData){
    console.log('comment onload called');
  }

  ngOnInit() {
    console.log('ngOnInit');
    this.comment = this.params.context.comment;
    if (isAndroid) {
      let inputInstance = this.comment_entry;
     // inputInstance.nativeElement.android.setSelection(this.comment, this.comment.length);
    } else if (isIOS) {

    }

  }

}
