require("nativescript-dom");
import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '~/app/services/user.service';
import { NetworkServiceService } from '~/app/services/network-service.service';
import { Assessment } from '~/app/models/assessment.model';
import { LoadingIndicator } from "nativescript-loading-indicator";
import { Student } from '~/app/models/student.model';
import { SearchBar } from "tns-core-modules/ui/search-bar";
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';
import { Answer } from '~/app/models/answer.model';
import { forEach } from '@angular/router/src/utils/collection';
import { Page, EventData } from "tns-core-modules/ui/page";
import { ExamitemComponent } from './examitem/examitem.component';
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout"
import * as _ from "lodash";
import { ModalDialogOptions, ModalDialogService } from 'nativescript-angular/modal-dialog';
import { StudentlistComponent } from './modals/studentlist/studentlist.component';
import { CommentEntryComponent } from './modals/comment-entry/comment-entry.component';


@Component({
  selector: 'ns-examdetail',
  templateUrl: './examdetail.component.html',
  styleUrls: ['./examdetail.component.css'],
  moduleId: module.id,
})

export class ExamdetailComponent implements OnInit {


  isBusy: Boolean = true;

  public assessmentTemplate: Assessment;
  public currentAssessment: Assessment;

  public answer: Answer[] = [];


  public studentlist: Student[] = [];

  public displayedStudentList = new ObservableArray<Student>(this.studentlist);


  examEnabled = false;

  examValid = false;

  selectedStudent: Student;

  // public assessment:Assessment = {"id":'1',"name":"an exam","description":"description","notes":null,"status":"active",
  // "exam_instance_items":[{"id":'9',"exam_instance_id":'1',"label":"another headingf",
  // "description":"this is another heading","order":0,"heading":"1","show_if_id":'-1',"show_if_answer_id":null,
  // "exclude_from_total":null,"no_comment":null,
  // "items":[]},
  // {"id":'1',"exam_instance_id":'1',"label":"Item label","description":"description","order":1,"heading":"","show_if_id":null,"show_if_answer_id":null,"exclude_from_total":null,"no_comment":null,
  // "items":[{"id":'1',"exam_instance_items_id":'1',"label":"S","description":"Satisfactory","value":'1',"order":'0',"needscomment":"false", selected:false}
  // ,{"id":'8',"exam_instance_items_id":'5',"label":"M","description":"Maybe","value":'1',"order":'2',"needscomment":"false", selected:false}]},
  // {"id":'8',"exam_instance_id":'1',"label":"heading label","description":"This is  a heading","order":3,"heading":"1","show_if_id":'-1',"show_if_answer_id":null,"exclude_from_total":null,"no_comment":null,"items":[]}]};

  constructor(
    private route: ActivatedRoute,
    public userservice: UserService, private networkService: NetworkServiceService,
    private page: Page,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef
  ) { }

  ngOnInit() { }

  viewLoaded(args: EventData) {
    console.log('viewLoaded called')
    //console.log(JSON.stringify(args.object))
    this.page = <Page>args.object;
    //console.log('lodash version:', _.VERSION);
    this.getAssessment();
  }

  onNavigatingTo(args: EventData) {
    console.log('onNavigatingTo called')
  }

  getAssessment() {
    console.log("examilst.component.ts says: getAssessment() called");
    this.answer = [];
    const id = this.route.snapshot.paramMap.get('id');
    this.loader.show(this.options);
    this.networkService.getAssessment(id)
      .subscribe(result => {
        // // get the template
        this.assessmentTemplate = result;
        // clone the assessment 
        this.currentAssessment = _.cloneDeep(this.assessmentTemplate);
        this.loader.hide();
      }
      ),
      error => console.log(error),
      () => console.log('examdetail.component.ts.getAssessments() subscription complete result:complete');

  }

  // getting then showing the student list
  getStudentList() {
    const id = this.route.snapshot.paramMap.get('id');
    this.studentlist = [];
    this.loader.show({ message: 'Getting students', });
    console.log("examilst.component.ts says: getAssessment() called");
    this.networkService.getstudentsForAssessment(id)
      .subscribe(result => {
        // this.assessment = result;
        console.log('get studentlist result is:' + JSON.stringify(result));
        // get student list  
        result.forEach(element => {
          console.log('Element:' + JSON.stringify(element));
          this.studentlist.push(element);
        });
        // this.displayedStudentList = new ObservableArray<Student>(this.studentlist);
        this.loader.hide();
        this.showStudentSelectDialog()
      }

      ),
      error => console.log(error),
      () => console.log('examdetail.component.ts.openUserSelect() subscription complete result:complete');

  }

  // show the student select dialog
  public showStudentSelectDialog() {
    let options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: true,
      animated: true,
      context: { studentlist: this.studentlist }
    };
    var parent = this;
    this.modalService.showModal(StudentlistComponent, options)
      .then((dialogResult: Student) => this.setStudent(dialogResult)
      );
  }

  // set the selected student
  setStudent(student) {
    if (student) {
      this.selectedStudent = student;
      this.examEnabled = true;
    }
  }

  // 
  public showCommentEntry(event) {
    console.log('showcommententry')
    let itemid = event.id;
    let oldcomment = event.comment;
    let options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      animated: true,
      //fullscreen: true,
      context: { comment: oldcomment }
    };
    this.modalService.showModal(CommentEntryComponent, options)
      .then((dialogResult: string) => this.setComment(itemid, dialogResult));
  }

  setComment(id, newcomment) {
    console.log('setting comment ' + id + ' to ' + newcomment)
    this.currentAssessment.exam_instance_items[this.currentAssessment.exam_instance_items.findIndex(x => x.id == id)].comment = newcomment;

  }

  // checking exam
  validateExam(event) {
    console.log('examdetail.component.ts says: validateExam()');
    console.log('event.id is:' + event.id);
    // record the answer in the answers array
    // find any affected element index, set visibility accordingly
    this.currentAssessment.exam_instance_items.forEach(element => {
      if (element.show_if_id == event.id) {
        console.log('Affected item:' + element.id)
        console.log('Looking for value:' + element.show_if_answer_id)
        if (element.show_if_answer_id == event.selectedid) {
          this.currentAssessment.exam_instance_items[this.currentAssessment.exam_instance_items.findIndex(x => x.id == element.id)].visible = true;
        } else {
          this.currentAssessment.exam_instance_items[this.currentAssessment.exam_instance_items.findIndex(x => x.id == element.id)].visible = false;
        }
      }
    });
    // loop through and see if everything's valid
    var test = true;
    this.currentAssessment.exam_instance_items.forEach(element => {
      console.log(element.valid ? 'Element is valid' : 'Element is n valid');
      if ((!element.heading) && (!element.valid)) {
        test = false;
      }
    });
    this.examValid = test;
    console.log(this.examValid ? 'Exam is valid' : 'Not valid');
  }

  // submit the assessment to the service
  submit() {
    this.loader.show();
    this.networkService.submitAssessment(this.selectedStudent.id, this.currentAssessment)
      .subscribe(result => {
        console.log('get studentlist result is:' + JSON.stringify(result));

        this.loader.hide();
        // reset here
        // disable exam
        this.examEnabled = false;
        // reset exam container
        this.currentAssessment = _.cloneDeep(this.assessmentTemplate);
        // reset student
        this.selectedStudent = null;
      }

      ),
      error => console.log(error),
      this.loader.hide();
      () => console.log('examdetail.component.ts.submit() submission complete result:complete');
  }


  // loading indicator
  loader = new LoadingIndicator();
  options = {
    message: 'Loading assessment detail',
    android: {
      indeterminate: true,
      cancelable: true,
      cancelListener: function (dialog) { console.log("Loading cancelled") },
    },
    ios: {
      details: "Please wait...",
      margin: 10,
      dimBackground: true,
      color: "#4B9ED6", // color of indicator and labels
    }
  };



}
