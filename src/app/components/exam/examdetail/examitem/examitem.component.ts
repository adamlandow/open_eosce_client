import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { AssessmentItemItem } from '~/app/models/assessmentitemitem.model';
import { AssessmentItem } from '~/app/models/assessmentitem.model';
import { RadioOption } from '~/app/components/exam/examdetail/examitem/radio-option';
import { FormGroup } from '@angular/forms';
import { View } from 'tns-core-modules/ui/page/page';
import { TextView } from 'tns-core-modules/ui/text-view';
import { AndroidData, ShapeEnum } from 'nativescript-ng-shadow';

@Component({
  selector: 'ns-examitem',
  templateUrl: './examitem.component.html',
  // styleUrls: ['./examitem.component.css'],
  moduleId: module.id,
})
export class ExamitemComponent extends View implements OnInit {

  formGroup: FormGroup;
  checkTest: boolean;

  show: boolean = true;

  assessmentitem: AssessmentItem = new AssessmentItem();

  //@Input() assessmentitem: AssessmentItem; = new AssessmentItem();
  @Input()
  get item() {
    return this.assessmentitem;
  }

  @Output() itemChange = new EventEmitter();
  set item(value) {
    this.assessmentitem = value;
  }

  //  @Input() comment: String;

  @Input() enabled: boolean = false;

  // is this a heading?
  isheading: Boolean = false;

  // formative?
  formative: Boolean = false;

  //hide comments?
  hidecomments: Boolean = false;

  // Internal values 
  selectedvalue: string = '';
  selectedid: number;
  comment: string = '';

  // does this 
  needscomment: boolean = false;

  // iOS
  shadowcolor = '#c4c4c4';
  // Android
  bgcolor = '#ffffe6'

  // tell the world something's changed, trigger validation
  @Output() changeEvent = new EventEmitter();

  @Output() commentEvent = new EventEmitter();

  radioOptions?: Array<RadioOption> = [];

  constructor() {
    super();
  }


  ngOnInit() {
    //console.log('No comment?' + this.assessmentitem.no_comment);
    console.log('ExamitemComponent.ngOnInit called');
    this.hidecomments = (this.assessmentitem.no_comment == '1');
    this.assessmentitem.items.forEach(item => {
      this.radioOptions.push(new RadioOption(item.id, item.label, item.value, item.needscomment == 'true'));
    });
    console.log('show?' + this.assessmentitem.show_if_id);
    if ((this.assessmentitem.show_if_id === null) || (this.assessmentitem.show_if_id < 0)) {
      this.assessmentitem.visible = true;
    } else {
      this.assessmentitem.visible = false;
    }
    this.assessmentitem.valid = false;
  }


  // handle a radio button change
  changeCheckedRadio(radioOption: RadioOption): void {
    // uncheck all other options
    console.log('Radio change: ')
    this.radioOptions.forEach(option => {
      if (option.value !== radioOption.value) {
        option.selected = false;
      } else {
        option.selected = true;
        // internal value
        this.selectedvalue = option.value;
        // external (bindable) value
        this.assessmentitem.selectedvalue = option.value;
        //console.log('selectedvalue is now: ' + this.selectedvalue)
        //this.selectedid = option.id;
        this.assessmentitem.selected_id = option.id;
        console.log('selectedid is now: ' + this.assessmentitem.selected_id);
        console.log('needs comment: ' + option.needscomment);
        if (option.needscomment) {
          console.log('Needs a comment');
          this.needscomment = true;

          if (this.assessmentitem.comment) {
            if (this.assessmentitem.comment.length > 0) {
              this.shadowcolor = "#c4c4c4";
              this.bgcolor = "#ffffe6";

            }
          } else {
            //  this.needscomment = false;
            this.shadowcolor = "#f2dede";
            this.bgcolor = "#f2dede";
          }
        } else {
          this.needscomment = false;
          this.shadowcolor = "#c4c4c4";
          this.bgcolor = "#ffffe6";
        }
      }
    });


    console.log('ExamitemComponent.Radio option changed to:' + this.selectedvalue)
    // check if this is valid or not
    this.assessmentitem.valid = this.validate();

    // tell the world the value has changed
    this.changeEvent.emit({ id: this.assessmentitem.id, selectedvalue: this.assessmentitem.selectedvalue, selectedid: this.assessmentitem.selected_id, comment: this.assessmentitem.comment });
  }

  // handle the change of comment
  notifyTextChange() {
    console.log('ExamitemComponent.Text changed to:' + this.comment)
    this.assessmentitem.comment = this.comment;
    // check if this is valid or not
    this.assessmentitem.valid = this.validate();
    this.changeEvent.emit({ id: this.assessmentitem.id, selectedvalue: this.assessmentitem.selectedvalue, selectedid: this.assessmentitem.selected_id, comment: this.assessmentitem.comment });
  }

  // notify that a comment box has been clicked
  notifyCommentClick() {
    console.log('COmment tapped: id:' + this.assessmentitem.id);
    this.commentEvent.emit({ id: this.assessmentitem.id, comment: this.assessmentitem.comment });
  }

  validate() {
    if (this.assessmentitem.selected_id) {
      if (this.needscomment) {
        if (this.assessmentitem.comment) {
          if (this.assessmentitem.comment.length > 0) {
            return true;
          } else {
            return false;
          }
        }
      } else {
        return true;
      }
    } else {
      return false;
    }
  }



}


