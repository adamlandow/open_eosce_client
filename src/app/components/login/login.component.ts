import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { LoginService } from "../../services/login.service"
import { LoadingIndicator } from "nativescript-loading-indicator";
import { Config } from '~/app/models/config.model';
import { Couchbase } from 'nativescript-couchbase';
import { User } from '~/app/models/user.model';
import { NetworkServiceService } from '~/app/services/network-service.service';
import { RouterExtensions } from 'nativescript-angular/router';
import { UserService } from '~/app/services/user.service';
import { ModalDialogOptions, ModalDialogService } from 'nativescript-angular/modal-dialog';
import { CommentEntryComponent } from '../exam/examdetail/modals/comment-entry/comment-entry.component';
import { AssessmentItem } from '~/app/models/assessmentitem.model';



@Component({
  selector: 'ns-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  moduleId: module.id,
})

export class LoginComponent implements OnInit {


  username: string = 'user1';
  password: string = 'password';
  loginResult: object = {};
  userResult: object = {};
  config: Config;
  user: User;
  storageDatabase: Couchbase;
  public isBusy = false;

  // testing
  assessmentitem: AssessmentItem = new AssessmentItem();

  constructor(private networkService: NetworkServiceService,
    public userService: UserService,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private routerExtensions: RouterExtensions
  ) {
    this.storageDatabase = new Couchbase('storage');
    //this.storageDatabase = new Couchbase('storage');
    //this.config =this.storageDatabase.getDocument("config")

    this.config = networkService.config;
    if (!this.config) {
      this.routerExtensions.navigate(['config']);
    } else {
     // this.login();
    }
    //this.user = new User();


  }

  ngOnInit() {
    console.log('login.component.ngOnInit')

  }

  login() {
    this.loader.show();
    console.log("login.component.ts says: login() called");
    this.networkService.login(this.username, this.password)
      .subscribe(observableresult => {
        console.log('login.component.ts.login() subscription result:' + observableresult['access_token']);
        this.networkService.access_token = observableresult['access_token'];
        this.networkService.getUserDetails()
          .subscribe(result => {
            this.loader.hide();
            // if (!this.storageDatabase.getDocument("user")) {
            this.user = new User();
            this.user.id = result['id'];
            this.user.name = result['name'];
            this.user.type = result['type'];
            this.user.currentToken = this.networkService.access_token;
            this.userService.user = this.user;
            //this.storageDatabase.createDocument(this.user, 'user');
            //  }
            // if (!this.config) {
            this.routerExtensions.navigate(['examlist']);
            // }
            //  for (let key in result) {
            //    console.log(key);
            //  };
          })

      }),
      error => console.log(error),
      () => console.log('login.component.ts.login() subscription complete result:complete');
    this.isBusy = false;
  }

  loader = new LoadingIndicator();
  options = {
    message: 'Logging in',
    android: {
      indeterminate: true,
      cancelable: true,
      cancelListener: function (dialog) { console.log("Login cancelled") },
    },
    ios: {
      details: "Please wait...",
      margin: 10,
      dimBackground: true,
      color: "#4B9ED6", // color of indicator and labels
    }
  };

  // experiment- modal screen
  // 
  public showCommentEntry(event) {
    console.log('showcommententry')
    let itemid = '42';
    let oldcomment = 'an old comment';
    let options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      animated: true,
      //fullscreen: true,
      context: { comment: oldcomment }
    };
    this.modalService.showModal(CommentEntryComponent, options)
      .then((dialogResult: string) => console.log('itemid:' + itemid + ' dialogResult:' + dialogResult));
  }

}
