import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { BarcodeScanner } from 'nativescript-barcodescanner';
import { Config } from '~/app/models/config.model';
import { Couchbase } from "nativescript-couchbase";
import { NetworkServiceService } from '~/app/services/network-service.service';
// import { registerElement } from "nativescript-angular/element-registry";
// registerElement("BarcodeScanner", () => require("nativescript-barcodescanner").BarcodeScannerView);

@Component({
  selector: 'ns-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css'],
  moduleId: module.id,
})

export class ConfigComponent implements OnInit {

  config: Config =new Config();
  storageDatabase: Couchbase;
  errormessage: string = '';
  observableresult: object = {};

  constructor(private barcodeScanner: BarcodeScanner, private networkService:NetworkServiceService,
    private route: ActivatedRoute) {
    this.storageDatabase = new Couchbase('storage');
    if (this.storageDatabase.getDocument("config")) {
      console.log('current config:'+JSON.stringify(this.storageDatabase.getDocument("config")));
      this.config = this.storageDatabase.getDocument("config");
    }
    // this.update();
    // this.query();
  }

  public update() {
    if (this.storageDatabase.getDocument("config")) {
      this.storageDatabase.updateDocument("config", this.config);
    } else {
      this.storageDatabase.createDocument(this.config, 'config');
    }
  }



  ngOnInit() {
  }



  scanBarcode() {
    console.log('config.component.ts scanBarcode called');
    this.barcodeScanner.requestCameraPermission().then(
      () => {
        console.log("Camera permission requested");
        this.barcodeScanner.scan({
          formats: "QR_CODE, EAN_13",
          cancelLabel: "EXIT. Also, try the volume buttons!", // iOS only, default 'Close'
          cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
          message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
          showFlipCameraButton: true,   // default false
          preferFrontCamera: false,     // default false
          showTorchButton: true,        // default false
          beepOnScan: false,             // Play or Suppress beep on scan (default true)
          torchOn: false,               // launch with the flashlight on (default false)
          closeCallback: () => { console.log("Scanner closed") }, // invoked when the scanner was closed (success or abort)
          resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
          // orientation: orientation,     // Android only, default undefined (sensor-driven orientation), other options: portrait|landscape
          openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
        }).then((result) => {
          // Note that this Promise is never invoked when a 'continuousScanCallback' function is provided
          console.log("Format: " + result.format + ",\nValue: " + result.text);
          try {
            let resultObj = JSON.parse(result.text);
            console.log("Result object: " + resultObj);
            if ((!!resultObj.service_url) && (!!resultObj.name) && (!!resultObj.client_id) && (!!resultObj.client_secret)) {
              this.config.name = resultObj.name;
              this.config.service_url = resultObj.service_url;
              this.config.client_id = resultObj.client_id;
              this.config.client_secret = resultObj.client_secret;
              this.update();
            } else {
              console.log('Not an eosce config code');
              this.errormessage = 'Not a valid Open eOSCE config code';
            }
          }
          catch (e) {
            console.log(e)
          }

          // this.config = 
          alert({
            title: "Scan result",
            message: "Format: " + result.format + ",\nValue: " + result.text,
            okButtonText: "OK"
          });
        }, (errorMessage) => {
          console.log("No scan. " + errorMessage);
        });
      }
    );

  }

  checkConfig() {
    this.networkService.checkConfig()
       .subscribe(observableresult => {
         console.log('config.component.ts.checkConfig() subscription result:' + observableresult['access_token']);
         this.observableresult = observableresult;
         for (let key in observableresult) {
           console.log(key);
         };
       }),
       error => console.log(error),
       () => console.log('config.component.ts.checkConfig() subscription complete result:complete');
  }

  public showHelp(helptext){
    let alertOptions = {
      title: "Help",
      message: helptext,
      okButtonText: "OK",
      cancelable: true // [Android only] Gets or sets if the dialog can be canceled by taping outside of the dialog.
  };
    alert(alertOptions);
  }

  public onScanResult(evt) {
    // console.log(evt.object);
    console.log(`onScanResult: ${evt.text} (${evt.format})`);
  }

}
