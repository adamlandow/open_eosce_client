import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import {LoginComponent} from "./components/login/login.component";
import {ConfigComponent} from "./components/config/config.component";
import { ExamlistComponent } from "./components/exam/examlist/examlist.component";
import { ExamdetailComponent } from "./components/exam/examdetail/examdetail.component";


const routes: Routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    {
        path: 'login',
        component: LoginComponent,
        data: { title: 'Login' }
      },
      {
        path: 'config',
        component: ConfigComponent,
        data: { title: 'Config' }
      },
      {
        path: 'examlist',
        component: ExamlistComponent,
        data: { title: 'Exam list' }
      },
      {
        path: 'examdetail/:id',
        component: ExamdetailComponent,
        data: { title: 'Exam Detail' }
      },
    { path: "home", loadChildren: "~/app/home/home.module#HomeModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
