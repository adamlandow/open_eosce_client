# Open eOSCE client
### This is the mobile client app for Open eOSCE.
##### Please find the administrative backend at https://bitbucket.org/adamlandow/open-eosce

Open eOSCE allows you to mark OSCE's created using the Open eOSCE application.

Open eOSCE is a way of conducting and managing Clincal Skills Assessments (or any skills assessment, really) electronically, rather than a paper-based format. The aims of Open eOSCE are to:

- Reduce administrative workload
- Reduce errors in data capture
- Provide timely feedback to participants
- Allow an OSCE to be conducted across multiple locations simultaneously
- Allow simplified quantitative and qualitative analysis of results for quality assurance and improvement

Open eOSCE is based upon lessons learned from developing UNE eOSCE, a project developed and supported by the School of Rural Medicine at the University of New England, Armidale. 

Open eOSCE client is built using NativeScript and Angular.

